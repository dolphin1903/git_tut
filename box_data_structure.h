/*
 * box_data_structure.h
 *
 *  Created on: Dec 11, 2018
 *      Author: haitn
 */

#ifndef COMMON_BOX_DATA_STRUCTURE_H_
#define COMMON_BOX_DATA_STRUCTURE_H_

#include <stdint.h>
#include "defines.h"

typedef struct message_header_t {
	uint32_t message_id; /** messages_id = sensor_id (1 byte) | box_id (24 bytes) **/
	uint16_t size; /** Include 4 bytes of checksum **/
} message_header;

typedef struct  {
	message_header msg_header;
	byte data[MAX_MSG_DATA_LENGTH];
	uint32_t crc32;
} message_t;

typedef struct  {
	int32_t value;
} vibration_sensor_t;

typedef struct acceleration_sensor_t {
    float angle_x;
    float angle_y;
    float angle_z;
	int16_t accel_x;
	int16_t accel_y;
	int16_t accel_z;
} acceleration_sensor_t;

typedef struct pressure_sensor_t {
	double altitude;
	double traveled_distance;
	uint16_t current_level;
	pressure_sensor_t() {
		altitude = 0;
		current_level = 0;
		traveled_distance = 0;
	}
} pressure_sensor_t;

typedef struct {
	float loading_value;
} load_sensor_t;

typedef struct {
	float tension_values[4];
} rope_tension_sensor_t;

typedef struct  {
	uint8_t status;
} door_status_t;

#endif /* COMMON_BOX_DATA_STRUCTURE_H_ */

#include "box_data_structure.h"

#define DEBUG (0)

char buffer[128];
message_t msg;

uint32_t box_id;
byte sensor_id;

pressure_sensor_t pressure_info;
acceleration_sensor_t accel_info;
load_sensor_t load_info;
vibration_sensor_t vibration_info;
door_status_t door_info;

#if (DEBUG == 1)
message_t testMsg;
pressure_sensor_t test_pressure_info;
acceleration_sensor_t test_accel_info;
load_sensor_t test_load_info;
#endif

bool need_update = true;

void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  Serial1.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  if (Serial1.available()) {
    Serial1.readBytes(buffer, 128);
    Serial.println(buffer);}
//    memcpy(&msg, buffer, 26);
//
//
//    box_id = (msg.msg_header.message_id & 0x00FFFFFF);
//    sensor_id = msg.msg_header.message_id >> 24;
//
//    Serial.print("BOX: ");
//    Serial.println(box_id);
//
//    switch (sensor_id) {
//      case 1:
//        memcpy(&pressure_info, msg.data, sizeof(pressure_sensor_t));
//        need_update = true;
//        break;
//      case 2:
//        memcpy(&accel_info, msg.data, sizeof(acceleration_sensor_t));
//        need_update = true;
//        break;
//      case 3:
//        memcpy(&load_info, msg.data, sizeof(load_sensor_t));
//        need_update = true;
//        break;
//      case 4:
//        memcpy(&vibration_info, msg.data, sizeof(vibration_sensor_t));
//        need_update = true;
//        break;
//      case 5:
//        memcpy(&door_info, msg.data, sizeof(door_status_t));
//        need_update = true;
//        break;
//      default:
//        need_update = false;
//        break;
//    }
//
//    if (need_update) {
//      Serial.print("Altitude: ");
//      Serial.print(pressure_info.altitude);
//      Serial.println(" m");
//      Serial.print("Travelled distance: ");
//      Serial.print(pressure_info.traveled_distance);
//      Serial.println(" m");
//      Serial.print("Current level: ");
//      Serial.println(pressure_info.current_level);
//  
//      Serial.print("Angle X: ");
//      Serial.print(accel_info.angle_x);
//      Serial.print(" degree, Angle Y: ");
//      Serial.print(accel_info.angle_y);
//      Serial.print(" degree, Angle Z: ");
//      Serial.print(accel_info.angle_z);
//      Serial.println(" degree");
//  
//      Serial.print("Accel X: ");
//      Serial.print(accel_info.accel_x);
//      Serial.print(" m/s2, Accel Y: ");
//      Serial.print(accel_info.accel_y);
//      Serial.print(" m/s2, Accel Z: ");
//      Serial.print(accel_info.accel_z);
//      Serial.println(" m/s2");
//  
//      Serial.print("Load: ");
//      Serial.print(load_info.loading_value);
//      Serial.println(" kg");
//
//      Serial.print("Vibration: ");
//      Serial.print(vibration_info.value);
//      Serial.println(" ohm");
//
//      Serial.print("Door: ");
//      if (0 == door_info.status) {
//        Serial.println(" open");
//      } else {
//        Serial.println(" close");
//      }
//      Serial.println("====================================");
//    }
//  }
//
//#if (DEBUG == 1)
//  // TEST
//  if (1 == gear) {
//    testMsg.msg_header.message_id = (0x01000000);
//    test_pressure_info.altitude = 100.25;
//    test_pressure_info.traveled_distance = 200.75;
//    test_pressure_info.current_level = 5;
//    memcpy(testMsg.data, &test_pressure_info, sizeof(pressure_sensor_t));
//    Serial1.write((uint8_t*)&testMsg, sizeof(message_t));
//  } else if (2 == gear) {
//    test_accel_info.angle_x = 1.5;
//    test_accel_info.angle_y = 2.5;
//    test_accel_info.angle_z = 3.5;
//    test_accel_info.accel_x = 10;
//    test_accel_info.accel_y = 20;
//    test_accel_info.accel_z = 30;
//    memcpy(testMsg.data, &test_accel_info, sizeof(acceleration_sensor_t));
//    testMsg.msg_header.message_id = (0x02000000);
//    Serial1.write((uint8_t*)&testMsg, sizeof(message_t));    
//  } else if (3 == gear) {
//    test_load_info.loading_value = 12.34;
//    memcpy(testMsg.data, &test_load_info, sizeof(load_sensor_t));
//    testMsg.msg_header.message_id = (0x03000000);
//    Serial1.write((uint8_t*)&testMsg, sizeof(message_t));    
//  }
//#endif

//  delay(2000);
}
